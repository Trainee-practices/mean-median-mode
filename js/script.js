const statistics = [];
const data = [12, 45, 17, 7, 22, 71, 50, 37, 10, 48, 17, 51, 17, 30, 12, 81];
data.sort((a, b) => a - b);

const mean = data.reduce((counter, num) => counter += num) / data.length;

/*Median formula:
* Odd: (n+1)/2
* Even: ((n/2) + ((n/2) + 1))/2
* Ternary operator to determine whether the lenght is odd or even to calculate the median */
const median = data.length % 2 === 0 ? (data[data.length / 2 - 1] + data[data.length / 2]) : data[Math.floor(data.length / 2)];
const frequency = data.reduce((rep, num) => (rep[num] ? rep[num] += 1 : rep[num] = 1, rep), {})
let maxFreq = 1;
let modeKey = 0;
let mode = Object.keys(frequency).reduce((counter, key) => {
    if(frequency[key] > maxFreq){
        maxFreq = frequency[key];
        modeKey = parseInt(key)
    }
    return modeKey;
});

statistics.push({mean, median, mode})
console.log(statistics);
